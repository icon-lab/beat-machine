# Beat-machine

Beat-machine é uma maquina de fazer beats no seu navegador.

## Instalação

Para a instalação você vai precisar do [Node.js](https://nodejs.org/en/) e o [XAMPP](https://www.apachefriends.org/index.html) (ou similar) instalados em seu computador. Após isso faça o ```git clone ``` deste projeto e dentro da pasta execute o ``` npm install``` para instalar as dependências. Para rodar a aplicação, execute-a via ```localhost```.

## Utilização

Use as teclas C V B N M do seu teclado para tocar os samples. Z remove a última sample adicionada, X limpa toda a sequência e SPACE inicia/para o playback. SHIFT habilita o metronomo caso precise. Se desejar, alterne entre as cenas para ouvir diferentes conjuntos de samples. 

## Contribuições e Agradecimentos

Este projeto foi baseado no projeto [108](https://github.com/hatsumatsu/108) de Martin Wecke e utiliza a biblioteca [canvid.js](https://gka.github.io/canvid/).

## Licenças

[MIT](https://choosealicense.com/licenses/mit/)